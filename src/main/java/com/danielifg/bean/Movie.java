/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielifg.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Daniel
 */
@Named
@RequestScoped
public class Movie implements Serializable {

    private String title;
    private String rate;
    private String releaseDate;
    private String runTime;
    private String genre;
    private String studio;
    private String image;

  
    ArrayList<String> timesList = new ArrayList();
    ArrayList<String> actorsList = new ArrayList();
    ArrayList<String> directorsList = new ArrayList();
    ArrayList<String> producersList = new ArrayList();
    ArrayList<String> writersList = new ArrayList();
    
      public String getImage() {
        return image;
    }
    

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getStudio() {
        return studio;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }

    public ArrayList<String> getTimesList() {
        return timesList;
    }

    public void setTimesList(ArrayList<String> timesList) {
        this.timesList = timesList;
    }

    public ArrayList<String> getActorsList() {
        return actorsList;
    }

    public void setActorsList(ArrayList<String> actorsList) {
        this.actorsList = actorsList;
    }

    public ArrayList<String> getDirectorsList() {
        return directorsList;
    }

    public void setDirectorsList(ArrayList<String> directorsList) {
        this.directorsList = directorsList;
    }

    public ArrayList<String> getProducersList() {
        return producersList;
    }

    public void setProducersList(ArrayList<String> producersList) {
        this.producersList = producersList;
    }

    public ArrayList<String> getWritersList() {
        return writersList;
    }

    public void setWritersList(ArrayList<String> writersList) {
        this.writersList = writersList;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.title);
        hash = 59 * hash + Objects.hashCode(this.rate);
        hash = 59 * hash + Objects.hashCode(this.releaseDate);
        hash = 59 * hash + Objects.hashCode(this.runTime);
        hash = 59 * hash + Objects.hashCode(this.genre);
        hash = 59 * hash + Objects.hashCode(this.studio);
        hash = 59 * hash + Objects.hashCode(this.timesList);
        hash = 59 * hash + Objects.hashCode(this.actorsList);
        hash = 59 * hash + Objects.hashCode(this.directorsList);
        hash = 59 * hash + Objects.hashCode(this.producersList);
        hash = 59 * hash + Objects.hashCode(this.writersList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movie other = (Movie) obj;
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.rate, other.rate)) {
            return false;
        }
        if (!Objects.equals(this.releaseDate, other.releaseDate)) {
            return false;
        }
        if (!Objects.equals(this.runTime, other.runTime)) {
            return false;
        }
        if (!Objects.equals(this.genre, other.genre)) {
            return false;
        }
        if (!Objects.equals(this.studio, other.studio)) {
            return false;
        }
        if (!Objects.equals(this.timesList, other.timesList)) {
            return false;
        }
        if (!Objects.equals(this.actorsList, other.actorsList)) {
            return false;
        }
        if (!Objects.equals(this.directorsList, other.directorsList)) {
            return false;
        }
        if (!Objects.equals(this.producersList, other.producersList)) {
            return false;
        }
        if (!Objects.equals(this.writersList, other.writersList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Movie{" + "title=" + title + ", rate=" + rate + ", releaseDate=" + releaseDate + ", runTime=" + runTime + ", genre=" + genre + ", studio=" + studio + ", timesList=" + timesList + ", actorsList=" + actorsList + ", directorsList=" + directorsList + ", producersList=" + producersList + ", writersList=" + writersList + '}';
    }

}
