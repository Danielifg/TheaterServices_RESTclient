/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielifg.Clientbackingbean;

import com.danielifg.bean.Movie;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.stream.XMLStreamException;
import org.w3c.dom.Document;

/**
 *
 * @author Daniel
 *
 * Class Controller to render the XML result from the DOM API parser
 */
@Named("BackingBean")
@RequestScoped
public class MoviesBackingBean implements Serializable {

    @Inject
    private Movie movie;

    private final Document doc = null;
    private String query;
    private String engine;
    private String message;
    private final String error;
    private XmlService xmlClient;

    public MoviesBackingBean() {
        this.error = "Movie not available";
    }

    public void displayMovie() throws XMLStreamException, FileNotFoundException, IOException {
        xmlClient = new XmlService();
        String response= xmlClient.getJson(engine, query);
        //Jackson in Acction!!
        ObjectMapper jacksonMapper = new ObjectMapper();
        movie = jacksonMapper.readValue(response, Movie.class);

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

}
